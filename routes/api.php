<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:api')->group(function () {
    Route::resource('/categories', 'CategoryController')->only(['index', 'store', 'destroy', 'update']);
    Route::resource('/boards', 'BoardController')->only(['index', 'store', 'destroy', 'update']);
    Route::resource('/boardLists', 'BoardListController')->only('index', 'store', 'update');
    Route::resource('/tasks', 'TaskController')->only(['store', 'update', 'destroy']);

    Route::get('/tasks/getBoardIdByTaskId/{task}', 'TaskController@getBoardIdByTaskId');

    Route::resource('/taskTimers', 'TaskTimerController')->only(['store', 'update', 'destroy']);
    Route::get('/taskTimers/getByTaskId/{task}', 'TaskTimerController@getByTaskId');


    Route::get('/lists/byBoard/{board}', 'BoardListController@index');
    Route::get('/startData', 'MainController@index');
    Route::put('/tasksDraggable', 'TaskController@updateAll');
    Route::get('/boardStatistics/{board}', 'BoardController@statistics');
});
Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});