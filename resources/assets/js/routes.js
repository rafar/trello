import ListOfBoards from './components/boards/ListOfBoards';
import BoardView from './components/boards/BoardView';
import BoardSettings from './components/boards/BoardSettings'
import BoardStatistics from './components/boards/BoardStatistics';
import TaskSettings from './components/tasks/TaskSettings'
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Categories from './components/categories/Categories';

export const routes = [
    {path: '/', component: ListOfBoards, name: 'home'},
    {path: '/login', component: Login, name: 'login'},
    {path: '/register', component: Register, name: 'register'},

    {path: '/categories', component: Categories, name: 'categories'},

    {path: '/boardView/:boardId', component: BoardView, name: 'boardView'},

    {path: '/boardSettings/:boardId', component: BoardSettings, name: 'boardSettings'},
    {path: '/boardStatistics/:boardId', component: BoardStatistics, name: 'boardStatistics'},

    {path: '/taskSettings/:taskId', component: TaskSettings, name: 'taskSettings'},
];