const state = {
    action: null
};

const getters = {
    action: (state) => {
        return state.action;
    },
};

const mutations = {
    setAction: (state, action) => {
        state.action = action;
    }
};

export default {
    state, mutations, getters
}