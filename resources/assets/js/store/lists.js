const state = {};

const getters = {};

const mutations = {};

const actions = {
    updateList(context, list) {
        return axios.put('/api/boardLists/' + list.id, list)
            .then(r => r.data)
            .then(response => {
            });
    }
};

export default {
    state, mutations, getters, actions
}