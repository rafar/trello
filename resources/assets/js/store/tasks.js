const state = {};

const getters = {
    taskById: (state, getters, rootState) => {
        return searchTaskId => {
            for (let boardIndex in rootState.board.boards) {
                let board = rootState.board.boards[boardIndex];
                if (typeof board.lists != 'undefined') {
                    for (let listIndex in board.lists) {
                        let list = board.lists[listIndex];
                        if (typeof list.tasks != 'undefined') {
                            for (let taskIndex in list.tasks) {
                                let task = list.tasks[taskIndex];
                                if (task.id == searchTaskId) {
                                    task.boardId = board.id;
                                    task.listId = list.id;
                                    return task;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};

const mutations = {
    addTask(state, payload) {
        payload.list.tasks.push(payload.task);
    },
};

const actions = {
    toggleTaskPriority(context, task) {
        task.priority = (task.priority + 1) % 3 + 1;
        context.dispatch('updateTask', task);
    },
    updateTask(context, task) {
       axios.put('/api/tasks/' + task.id, task)
            .catch(e => {
                console.log(e)
            });
    },
    toggleTaskStatus(context, task) {
        task.status = task.status == 'PENDING' ? 'DONE' : 'PENDING';
        context.dispatch('updateTask', task);
    },
    addTask(context, payload) {
        payload.board = context.getters.activeBoard;
        return axios.post('/api/tasks', {task: payload.task})
            .then(r => r.data)
            .then(response => {
                payload.task = response.task;
                context.commit('addTask', payload);
            })
            .catch(e => {
                console.log(e)
            });

    },
};

export default {
    state, mutations, getters, actions
}