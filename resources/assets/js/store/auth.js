const state = {
    isLoggedIn: !!localStorage.getItem('token'),
    user:  null
};

const getters = {
    isLoggedIn: (state) => {
        return state.isLoggedIn;
    },
};

const mutations = {
    loginUser(state, user) {
        state.isLoggedIn = true;
        state.user = user;
    },
    logoutUser(state) {
        state.isLoggedIn = false;
        state.user = null;
    }
};

export default {
    state, mutations, getters
}