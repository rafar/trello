import Vue from 'vue'
import Vuex from 'vuex'
import board from './board'
import tasks from './tasks'
import actions from './actions'
import taskTimers from './taskTimers'
import auth from './auth'
import category from './category'
import list from './lists'
import favicon from './favicon'

Vue.use(Vuex);

export const store = new Vuex.Store({
    plugins: [],
    modules: {board, actions, tasks, taskTimers, auth, category, list, favicon},
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    namespaced: true
});