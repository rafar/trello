<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTimersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_timers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->index()->unsigned();
            $table->timestamp('start')->nullable();
            $table->timestamp('stop')->nullable();
            $table->float('hours')->default(0);
            $table->date('date');
            $table->timestamps();
            $table->foreign('task_id')->references('id')->on('tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_timers');
    }
}
