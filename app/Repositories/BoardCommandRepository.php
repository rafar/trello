<?php

namespace App\Repositories;

use App\Board;
use App\Http\Requests\board\BoardStoreRequest;
use App\Services\cache\CacheConsts;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Psr\SimpleCache\InvalidArgumentException;

class BoardCommandRepository
{
    public function create(BoardStoreRequest $data): Board
    {
        $board = new Board();
        $board->name = $data->name;
        if ($data->category_id) {
            $board->category_id = (int)$data->category_id;
        }
        $board->user_id = \Auth::id();
        $board->save();
        $this->clearCacheAll();
        return $board;
    }

    public function update(Board $board, BoardStoreRequest $data): Board
    {
        $board->update($data->only('name', 'archived'));
        $this->clearCacheAll();
        return $board;
    }

    private function clearCacheAll()
    {
        try {
            Cache::delete(CacheConsts::getCacheKeyForId(CacheConsts::CACHE_ALL_BOARDS, auth()->user()->id));
            Log::debug( 'CACHE_ALL_BOARDS cleared');
        } catch (InvalidArgumentException $e) {
            Log::error('Cant clear cache for all boards');
        }
    }
}