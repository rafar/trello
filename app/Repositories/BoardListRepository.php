<?php

namespace App\Repositories;


use App\BoardList;
use App\Http\Requests\boardList\BoardListStoreRequest;

class BoardListRepository
{

    /**
     * @param BoardList $boardList
     * @param BoardListStoreRequest $data
     * @return BoardList
     */
    public static function update(BoardList $boardList, BoardListStoreRequest $data): BoardList
    {
        $boardList->update($data->only('name', 'archived', 'color'));
        return $boardList;
    }

}