<?php
/**
 * Created by PhpStorm.
 * User: dariusz
 * Date: 02.08.19
 * Time: 14:05
 */

namespace App\Repositories;


use App\BoardList;
use App\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class TaskQueryRepository
{
    /**
     * @param int $boardId
     * @return Collection|Task[]
     */
    public function getTasksByBoardId(int $boardId): Collection
    {
        return Task::whereHas('list', function (Builder $query) use ($boardId) {
            $query->where('board_id', $boardId);
        })->get();
    }
}