<?php

namespace App\Repositories;

use App\Board;
use App\Services\cache\CacheConsts;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class BoardQueryRepository
{

    /**
     * @return Collection|Board[]
     */
    public function getAll(): Collection
    {
        return Cache::remember(
          CacheConsts::getCacheKeyForId(CacheConsts::CACHE_ALL_BOARDS, auth()->id()),
          CacheConsts::WEEK,
          function () {
              return Board::all();
          });
    }
}