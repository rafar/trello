<?php
/**
 * Created by PhpStorm.
 * User: dariusz
 * Date: 02.08.19
 * Time: 14:58
 */

namespace App\Repositories;


use App\TaskTimer;
use Illuminate\Database\Eloquent\Collection;

class TaskTimerQueryRepository
{

    /**
     * @param array $tasksIds
     * @return Collection|TaskTimer[]
     */
    public function getTimersByTasksIds(array $tasksIds): Collection
    {
        return TaskTimer::query()->whereIn('task_id', $tasksIds)->get();
    }
}