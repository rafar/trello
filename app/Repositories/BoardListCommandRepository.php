<?php

namespace App\Repositories;

use App\BoardList;
use App\Services\cache\CacheConsts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Psr\SimpleCache\InvalidArgumentException;

class BoardListCommandRepository
{
    public function create(Request $request)
    {
        $boardList = new BoardList();

        $boardList->name = $request->list['name'];
        $boardList->board_id = (int)$request->list['id'];

        $boardList->save();
        $this->clearCacheAll();
        return $boardList;
    }

    private function clearCacheAll()
    {
        try {
            Cache::delete(CacheConsts::getCacheKeyForId(CacheConsts::CACHE_ALL_BOARDS_LISTS, auth()->user()->id));
            Log::debug('CACHE_ALL_BOARDS_LISTS cleared');
        } catch (InvalidArgumentException $e) {
            Log::error('cant delete boardlist cache');
        }
    }
}