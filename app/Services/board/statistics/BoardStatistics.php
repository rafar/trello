<?php

namespace App\Services\board\statistics;

use App\Repositories\TaskQueryRepository;
use App\Repositories\TaskTimerQueryRepository;

class BoardStatistics
{
    public function getTasksByDays(int $boardId): array
    {
        $tasksRepository = new TaskQueryRepository();
        $taskTimersRepository = new TaskTimerQueryRepository();

        $tasks = $tasksRepository->getTasksByBoardId($boardId)->keyBy('id');
        $taskTimers = $taskTimersRepository->getTimersByTasksIds($tasks->keys()->toArray());

        $result = [];

        foreach ($taskTimers as $taskTimer) {
            $timerDate = $taskTimer->date->toDateString();
            $taskName = $tasks[$taskTimer->task_id]->name;

            $taskDataOnDate = isset($result[$timerDate]) ? array_search($taskName, array_column($result[$timerDate], 'name')) : false;

            if (!isset($result[$timerDate]) || $taskDataOnDate === false) {
                $result[$timerDate][] = ['name' => $taskName, 'hours' => $taskTimer->hours];
            } else {
                $result[$timerDate][$taskDataOnDate]['hours'] += $taskTimer->hours;
            }
        }
        return $result ?? [];
    }

    /** TODO */
    public function getSumOfHoursByDaysByBoardId(int $boardId): array
    {

    }
}