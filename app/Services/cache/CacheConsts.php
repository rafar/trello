<?php

namespace App\Services\cache;

class CacheConsts
{
    const SEPARATOR = '-';

    const CACHE_ALL_BOARDS = 'boards';
    const CACHE_ALL_BOARDS_LISTS = 'boards-lists';

    const HOUR = 3600;
    const DAY = 86400;
    const WEEK = 604800;

    public static function getCacheKeyForId(string $key, int $id): string
    {
        return $key . self::SEPARATOR . $id;
    }
}