<?php

namespace App\Http\Controllers;

use App\Board;
use App\Category;
use App\Http\Resources\BoardResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\TaskTimerResource;
use App\TaskTimer;

class MainController extends Controller
{
    public function index()
    {
        return \Response::json([
          'boards' => BoardResource::collection(Board::orderByDesc('id')->with('category')->paginate(50)),
          'taskTimers' => TaskTimerResource::collection(TaskTimer::with('task')->whereStop(null)->paginate(99)),
          'categories' => CategoryResource::collection(Category::orderBy('name')->paginate(99))
        ]);
    }
}
