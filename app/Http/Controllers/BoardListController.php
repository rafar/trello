<?php

namespace App\Http\Controllers;

use App\Board;
use App\BoardList;
use App\Http\Requests\boardList\BoardListStoreRequest;
use App\Http\Resources\BoardListResource;
use App\Repositories\BoardListCommandRepository;
use App\Repositories\BoardListRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BoardListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Board $board)
    {
        return BoardListResource::collection(
          BoardList::with([
            'tasks' => function ($tasks) {
                return $tasks->with('taskTimers');
            }
          ])
            ->where('board_id', $board->id)
            ->orderBy('id')
            ->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $boardList = (new BoardListCommandRepository())->create($request);
        return Response::json(array('success' => true, 'id' => $boardList->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoardList $boardList
     * @return \Illuminate\Http\Response
     */
    public function show(BoardList $boardList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoardList $boardList
     * @return \Illuminate\Http\Response
     */
    public function edit(BoardList $boardList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BoardListStoreRequest $request
     * @param  \App\BoardList $boardList
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BoardListStoreRequest $request, BoardList $boardList)
    {
        $boardList = BoardListRepository::update($boardList, $request);
        return Response::json(array('success' => true, 'boardList' => $boardList), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoardList $boardList
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoardList $boardList)
    {
        //
    }
}
