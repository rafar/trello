<?php

namespace App;

use App\Repositories\BoardListQueryRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Task
 *
 * @property int $id
 * @property int $board_list_id
 * @property string $name
 * @property string $status
 * @property int $priority
 * @property int $order
 * @property string|null $deadline
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\BoardList $list
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereBoardListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskTimer[] $hoursSum
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskTimer[] $taskTimers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task query()
 */
class Task extends Model
{
    protected $fillable = ['order', 'board_list_id', 'priority', 'deadline', 'name', 'status'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('board_list_id', function (Builder $builder) {
              $builder->whereIn('board_list_id',
                (new BoardListQueryRepository())->getAll()->pluck('id')
                ->toArray());
        });
    }

    public function list(): BelongsTo
    {
        return $this->belongsTo('App\BoardList', 'board_list_id');
    }

    public function taskTimers()
    {
        return $this->hasMany('App\TaskTimer');
    }

    public function hoursSum()
    {
        return $this->hasMany('App\TaskTimer')
          ->selectRaw('SUM(hours) as hours, task_id')
          ->groupBy('task_id');
    }

}
